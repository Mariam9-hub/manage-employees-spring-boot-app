package com.firstApp.demotest.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.firstApp.demotest.model.Emplyee;

public interface EmployeeRepo extends JpaRepository<Emplyee, Long> {

	Optional<Emplyee> findEmplyeeById(Long id);

	void deleteEmlyeeById(long id);

	

}
