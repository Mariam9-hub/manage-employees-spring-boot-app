package com.firstApp.demotest;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.firstApp.demotest.model.Emplyee;
import com.firstApp.demotest.service.EmployeeService;

@RestController
@RequestMapping("/employee")
public class EmployeeResource {
	 public final EmployeeService employeeService;

	public EmployeeResource(EmployeeService employeeService) {
		this.employeeService = employeeService;
	}
	 
	@GetMapping("all")
	public ResponseEntity<List<Emplyee>> getAllEmployees(){
		List<Emplyee> employees = employeeService.findAllEmployees();
		return new ResponseEntity<>(employees, HttpStatus.OK);
	}
	
	@GetMapping("find/{id}")
	public ResponseEntity<Emplyee> getEmployeeById(@PathVariable("id") Long id){
		Emplyee employee = employeeService.findEmploeeById(id);
		return new ResponseEntity<>(employee, HttpStatus.OK);
	}
	
	@PostMapping("add")
	public ResponseEntity<Emplyee> addEmployee(@RequestBody Emplyee employee){
		Emplyee newEmployee = employeeService.addEmployee(employee);
		return new ResponseEntity<>(newEmployee, HttpStatus.CREATED);
	}
	
	@PutMapping("update")
	public ResponseEntity<Emplyee> updateEmployee(@RequestBody Emplyee employee){
		Emplyee updateEmployee = employeeService.updateEmployee(employee);
		return new ResponseEntity<>(updateEmployee, HttpStatus.OK);
	}
	
	@DeleteMapping("delete/{id}")
	public ResponseEntity<?> deleteEmployeeById(@PathVariable("id") Long id){
		employeeService.deleteEmployeeById(id);
		return new ResponseEntity<>( HttpStatus.OK);
	}

}
