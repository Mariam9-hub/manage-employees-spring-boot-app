package com.firstApp.demotest.service;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.firstApp.demotest.exception.UserNotFoundException;
import com.firstApp.demotest.model.Emplyee;
import com.firstApp.demotest.repo.EmployeeRepo;

@Service
public class EmployeeService {
	
	private final EmployeeRepo employeerepo;
	@Autowired
	public EmployeeService(EmployeeRepo employeerepo) {
		this.employeerepo = employeerepo;
	}
	
	public Emplyee addEmployee(Emplyee employee) {
		employee.setEmployeeCode(UUID.randomUUID().toString());
		return employeerepo.save(employee);
		
	}
	
	public Emplyee updateEmployee(Emplyee employee) {
		return employeerepo.save(employee);
		
	}
	
	public List<Emplyee> findAllEmployees(){
		return employeerepo.findAll();
	}
	
	public Emplyee findEmploeeById(Long id) {
		return employeerepo.findEmplyeeById(id).orElseThrow(()-> new UserNotFoundException("User by id "+ id +" was not found"));
	}
	
	public void deleteEmployeeById(long id) {
		employeerepo.deleteById(id);
	}
	
	
}
